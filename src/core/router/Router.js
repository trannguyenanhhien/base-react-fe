import * as React from "react";
import { Routes, Route } from "react-router-dom";
import { Counter } from "../../redux-toolkit/counter/Counter";
import MainLayout from "../main-layout/MainLayout";
import Products from "../../views/main/products/Products";
import CreateProducts from "../../views/main/products/CreateProduct";
import POS from "../../views/main/pos/POS";
import Test from "../../views/main/Test";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Test />} />
      <Route element={<MainLayout />}>
        <Route path="/products" element={<Products />}>
          <Route path="create-product" element={<CreateProducts />} />
        </Route>
        <Route path="pos" element={<POS />} />
      </Route>
    </Routes>
  );
};

export default Router;
