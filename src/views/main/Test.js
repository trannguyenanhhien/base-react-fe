import {Box, Button, Grid, Hidden} from '@mui/material';
import React, {useEffect, useState} from 'react';
import xml_data from './const';
import axios from 'axios';

const Test = () => {
  const solve = () => {
    var a = document
      .getElementById('a1')
      .outerHTML.replace(/&lt;/g, '<')
      .replace(/&gt;/g, '>')
      .replace(/block/g, 'temp53')
      .replace(/none/g, '')
      .replace(/temp53/g, 'none');
    console.log(a);
    let output = `
        <xsl:stylesheet version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        <xsl:template match="/HDon">
          <html lang="en">
          <body>
            <div id=${`"DLHDon/NDHDon/NBan"`} style="margin:20px;">
            ${a}
            </div>
          </body>
          </html>
        </xsl:template>
        </xsl:stylesheet>`;
    solve2(output);
  };
  const solve2 = (file) => {
    var blob = new Blob([`${file}`], {type: 'application/xml'});
    // const downloadUrl = window.URL.createObjectURL(blob);
    // const a = document.createElement('a');
    // a.href = downloadUrl;
    // a.setAttribute('download', 'TestXSL.xsl');
    // document.body.appendChild(a);
    // a.click();
    var file2 = new File([blob], 'mau_test.xsl', {
      type: 'application/xml',
    });
    const data = new FormData();
    data.append('xsl_file', file2);
    axios.post('https://testing-ei-ms.pvssolution.com/admin/templates/test-update-xsl', data, {responseType: 'blob'}).then((res) => {
      var file = new Blob([res.data], {type: 'application/pdf'});
      var fileURL = URL.createObjectURL(file);
      window.open(fileURL);
    });
  };
  const [styles, setStyles] = useState({
    header: {
      fontWeight: 500,
      textTransform: 'uppercase',
      color: 'red',
    },
  });
  return (
    <>
      <div style={{margin: 30}}>Test File XSL</div>
      <div id='a1'>
        <style>
          {`
            #main {
              background-image:url(watermark.png); 
              background-repeat:no-repeat;
              background-position: center 300px;
              background-size: auto 200px;
              padding:0;	
              margin-left:0;	
              width:100%;	
            }
            .labelNormalHeader {
              color: red;
            }
            .labelItalicHeader {
              color: blue
            }
            .show {
              display: block
            }
            .notShow {
              display: none
            }
            tbody{
            }
            .header{
            vertical-align:top
            }
            #section-to-print{	
              padding:0;	
              margin-left:0;	
              width:100%;	
            }
            .invoiceName{
            font-weight:bold;
            font-size:18pt;
            }
            .titleInvoice{
            font-weight:bold;
            font-style: italic;
            font-size:12pt;
            }
            .serif {
            font-family: "Times New Roman", Times, serif;
            }
            .sansserif {
            font-family: Arial, Helvetica, sans-serif;
            }
            .image-box {
            text-align:center;
            }
            .image-box img {
            <!--opacity: 0.9;-->
            width: 350px;
            background-image: none;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            margin-top:300px;
            margin-bottom: 100px;
            }
  
            img[src=""] {
            display: none;
            }
  
            .watermark {
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            position: absolute;
            z-index: -2;
            margin-left:auto;
            margin-right:auto;
            width:400px;
            margin-top: 0px;
            }
  
            .itemNormal{
            font-weight: normal;
            padding : 2px 2px 2px 2px;
            font-size:12pt;
            }
  
            .itemBold{
            font-weight:bold;
            /*vertical-align:top;*/
            padding : 2px 2px 2px 2px;
            font-size:12pt;
            }
            .labelNormal{
            padding : 2px 2px 2px 2px;
            font-size:12pt;
            }          
            .labelItalic{
            padding : 2px 2px 2px 2px;
            font-style: italic;
            color: #000000;
            font-size:12pt;
            }
  
            .labelItalicNormal{
            padding : 2px 2px 2px 2px;
            font-style: italic;
            font-weight: normal;
            color: #000000;
            font-size:12pt;
            }
  
            .labelBold{
            font-weight:bold;
            /*vertical-align:top;*/
            padding : 2px 2px 2px 2px;
            font-size:12pt;
            }
  
  
  
            .boxLarge{
            margin-left:auto;
            margin-right:auto;
            border-collapse: collapse;
            padding : 5px 5px 5px 5px;
            border: 5px solid #626262;
            width:880.267px;
            }
            .boxSmall{
            border-collapse: collapse;
            padding : 5px 5px 5px 5px;
            border: 1px solid;
            }
            .boxSmallTable{
            border-spacing: 0px;
            padding : 0px 0px 0px 0px;
            outline: 1px solid #626262;
            }
            .boxSmallTable .labelBold{	
              font-size:13px;	
            }	
            .boxSmallTable .labelItalic{	
              font-size:13px;	
            }
            .dataInfoInvoice{
            vertical-align:top;
            font-weight:bold;
            padding : 2px 2px 2px 2px
            }
            <!--Bat buoc phai co - dau hieu nhan biet de change color-->
            <!--Start_color-->
            .invoiceName{
            color: #000000;
            }
            .invoiceNameItalic{
            color: #000000;
            font-style: italic;
            }
            .titleInvoice{
            color: #000000;
            }
            .itemNormal{
            color: #000000;
            }
            .itemBold{
            color: #000000;
            }
            .labelNormal{
            color: #000000;
            }
            .labelBold{
            color: #000000;
            }
            .boxLarge{
            color: #000000;
            border-style: double;
            <!--border-width: medium;-->
            }
            .boxSmall{
            color: #000000;
            }
  
  
            .borderBottom{
            border-bottom: 2px solid #4C3F57;
            }
            .BG {
            <!--opacity: 0.3;-->
            background-image: url(signature.png);
            background-repeat: no-repeat;
            background-position: center top;
            background-size: 200px 70px;
            }
            img[src=""] {
            display: none;
            }
            .labelNormalHeader{
            font-size:10pt;
            }
            .labelBoldHeader{
            font-size:16pt;
            }
            .labelItalicHeader{
            font-style:italic;
            font-size:10pt;
            }
            .labelNormalHeader{
            color: #000000;
            }
            .labelBoldHeader{
            color: #000000;
            }
          `}
        </style>
        <table id='main' ALIGN='center' class='serif boxLarge'>
          <tr>
            <td width='25%' align='center' style={{padding: '3px 3px 3px 3px'}}>
              <font class='labelBoldHeader show' style={styles.header}>
                <img src='' />
              </font>
              <font class='labelBoldHeader notShow' style={styles.header}>
                {`<xsl:value-of select='DLHDon/NDHDon/NBan/LOGO' />`}
              </font>
            </td>
            <td colspan='2'>
              <table width='100%'>
                <tr>
                  <td colspan='3'>
                    <font class='labelBoldHeader show' style={styles.header}>
                      {`{TÊN CÔNG TY}`}
                    </font>
                    <font class='labelBoldHeader notShow' style={styles.header}>
                      {`<xsl:value-of select='DLHDon/NDHDon/NBan/Ten' />`}
                    </font>
                  </td>
                </tr>
                <tr>
                  <td colspan='3'>
                    <font class='labelNormalHeader'>Mã số thuế </font>
                    <font class='labelItalicHeader'>(Tax code): </font>
                    <font class='labelNormalHeader notShow' style={{fontWeight: 'bold'}}>
                      {`<xsl:value-of select='DLHDon/NDHDon/NBan/MST' />`}
                    </font>
                  </td>
                </tr>
                <tr>
                  <td colspan='3'>
                    <font class='labelNormalHeader'>Địa chỉ </font>
                    <font class='labelItalicHeader'>(Address): </font>
                    <font class='labelNormalHeader notShow'>{`<xsl:value-of select='DLHDon/NDHDon/NBan/DChi' />`}</font>
                  </td>
                </tr>
                <tr>
                  <td colspan='2'>
                    <font class='labelNormalHeader'>Điện thoại </font>
                    <font class='labelItalicHeader'>(Tel): </font>
                    <font class='labelNormalHeader notShow'>{`<xsl:value-of select='DLHDon/NDHDon/NBan/SDThoai' />`}</font>
                  </td>
                  <td width='60%'>
                    <font class='labelNormalHeader'>Fax: </font>
                    <font class='labelNormalHeader notShow'>{`<xsl:value-of select='DLHDon/NDHDon/NBan/Fax' />`}</font>
                  </td>
                </tr>
                <tr>
                  <td colspan='2'>
                    <font class='labelNormalHeader'>Email: </font>
                    <font class='labelNormalHeader notShow'>{`<xsl:variable name='FeatureInfo' select='DLHDon/NDHDon/NBan/DCTDTu' />`}</font>
                  </td>
                  <td width='60%'>
                    <font class='labelNormalHeader'>Website: </font>
                    <font class='labelNormalHeader notShow'>{`<xsl:value-of select='DLHDon/NDHDon/NBan/Website' />`}</font>
                  </td>
                </tr>
                <tr>
                  <td style={{verticalAlign: 'top'}} width='28%'>
                    <font class='labelNormalHeader'>Số tài khoản </font>
                    <font class='labelItalicHeader'>(Account No): </font>
                  </td>
                  <td width='16%' style={{verticalAlign: 'top'}}>
                    <font class='labelNormalHeader notShow'>{`<xsl:variable name='FeatureInfo' select='DLHDon/NDHDon/NBan/STKNHang' />`}</font>
                  </td>
                  <td width='60%' style={{verticalAlign: 'top'}}>
                    <font class='labelNormalHeader notShow'>tại {`<xsl:variable name='FeatureInfo' select='DLHDon/NDHDon/NBan/TNHang' />`}</font>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
      <button
        onClick={() => {
          setStyles({
            ...styles,
            header: {
              ...styles.header,
              fontWeight: styles.header.fontWeight === 'bold' ? 500 : 'bold',
            },
          });
        }}
      >
        In đậm
      </button>
      <Button onClick={solve}>Click</Button>
    </>
  );
};

export default Test;
